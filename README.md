# marrtino_programming

C++ to Python bridge for simple robot programming

## Detailed Description

This package contains only the source code of a library that exports basic C functions for programming a MARRtino robot using the orazio_core_library.

This library is needed to use the program app in [marrtino_apps](https://bitbucket.org/iocchi/marrtino_apps/)

## Prerequisites

* [srrg_orazio_core](https://gitlab.com/srrg-software/srrg_orazio_core)
 
## Installation

* Copy or link this folder in your catkin workspace and compile with catkin_make.