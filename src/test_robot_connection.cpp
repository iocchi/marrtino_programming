#include "orazio_robot_connection.h"

#include <string>
#include <iostream>
#include <cmath>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>

using namespace std;
using namespace srrg_orazio_core;

OrazioRobotConnection robot;
string serial_port = "/dev/ttyACM0";


int main() {

    cout << "Connecting to serial port: " << serial_port << " ... " << endl;
    robot.connect(serial_port.c_str());
    if (! robot.isConnected()){
        cerr << "ERROR: unable to open serial port [" << serial_port <<"]" << endl;
        return 0;
    }
    cout << "Connection OK" << endl;
    
	double tv=0, rv=0;

    while(true) {

		cout << "  == Robot thread running ..." << endl;
        
		cout << "  -- robot spin - set veocities " << tv << " " << rv << endl;
        robot.setBaseVelocities(tv,rv);
		cout << "  -- robot spin - set veocities DONE" << endl;
        robot.spinOnce();  // runs at 100 Hz
		cout << "  ++ robot spin ... " << endl;



#if 0
      	int ss = robot.systemSeq();
        if (ss%10==0)
            cout << "DEBUG: " << ss << " VEL: " << last_speed_tv << " " << last_speed_rv << "  POS: " << robot.x() << " " << robot.y() << " " << RAD2DEG(robot.theta()) << endl;
#endif

    }
	return 0;
}




