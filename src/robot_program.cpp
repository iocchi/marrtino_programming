#include "orazio_robot_connection.h"

#include <string>
#include <iostream>
#include <cmath>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>

using namespace std;
using namespace srrg_orazio_core;

#define DEG2RAD(a) ((a)*M_PI/180.0)
#define RAD2DEG(a) ((a)/M_PI*180.0)
#define NORM_PI(a) (((a)>M_PI)?((a)-2*M_PI):(((a)<-M_PI)?((a)+2*M_PI):(a)))


OrazioRobotConnection robot;
string serial_port = "/dev/ttyACM0";
bool run = true;

// Good tv rv values
double tv_good = 0.2;
double rv_good = 0.8;

double move_step = 0.5;

// Last vaue of speed send to the robot
double last_speed_tv=0, last_speed_rv=0;
// Desired target speed
double target_speed_tv=0, target_speed_rv=0;
// Units per tick / running at 100 Hz
double max_acct=0.001, max_dect=0.1, max_accr=0.01, max_decr=0.1;

bool isConnected = false;

bool connect() {
    cout << " -- Connecting to serial port: " << serial_port << " ... " << endl;
    robot.connect(serial_port.c_str());
    if (! robot.isConnected()){
        cerr << "ERROR: unable to open serial port [" << serial_port <<"]" << endl;
        return 0;
    }
    // usleep(1e6);
    cout << " -- Connection OK" << endl;
    isConnected = true;
    return 1;
}

extern "C" void setSpeed(double tv, double rv) {
    target_speed_tv = tv; target_speed_rv = rv;
}

extern "C" void stop() {
    target_speed_tv = 0; target_speed_rv = 0;
    last_speed_tv = 0; last_speed_rv = 0;
}

void wait(double sec) {
    usleep(sec*1e6);
}



void* run_thread(void *arg) {

    //cout << "Connecting to the robot ..." << endl;
    while (!connect()) {
        usleep(3e6);
    }
    
	cout << "Robot thread running ..." << endl;

    while(run) {

		// cout << "  == Robot thread running ..." << endl;
#if 0
        // Linear velocity
        // target: stop
        if (fabs(target_speed_tv)<0.001) {
            if (last_speed_tv>max_dect)
                last_speed_tv -= max_dect;
            else if (last_speed_tv<-max_dect)
                last_speed_tv += max_dect;
            else
                last_speed_tv = target_speed_tv;
        }
        else {
            // target: vel != 0
            if (target_speed_tv-last_speed_tv>max_acct)
                last_speed_tv += max_acct;
            else if (target_speed_tv-last_speed_tv<-max_acct)
                last_speed_tv -= max_acct;
            else
                last_speed_tv = target_speed_tv;
        }

        // Angular velocity
        // target: stop
        if (fabs(target_speed_rv)<0.001) {
            if (last_speed_rv>max_decr)
                last_speed_rv -= max_decr;
            else if (last_speed_rv<-max_decr)
                last_speed_rv += max_decr;
            else
                last_speed_rv = target_speed_rv;
        }
        else {
            if (target_speed_rv-last_speed_rv>max_accr)
                last_speed_rv += max_accr;
            else if (target_speed_rv-last_speed_rv<-max_accr)
                last_speed_rv -= max_accr;
            else
                last_speed_rv = target_speed_rv;
        }
#else
		last_speed_tv = target_speed_tv;
		last_speed_rv = target_speed_rv;
#endif

//		cout << "  -- robot spin - set veocities " << last_speed_tv << " " << last_speed_rv << endl;
        robot.setBaseVelocities(last_speed_tv,last_speed_rv);
//		cout << "  -- robot spin - set veocities DONE" << endl;
        robot.spinOnce();  // runs at 100 Hz
//		cout << "  ++ robot spin ... " << endl;

#if 0
      	int ss = robot.systemSeq();
        if (ss%10==0)
            cout << "DEBUG: " << ss << " VEL: " << last_speed_tv << " " << last_speed_rv << "  POS: " << robot.x() << " " << robot.y() << " " << RAD2DEG(robot.theta()) << endl;
#endif

    }
	return 0;
}


extern "C" void start_robot_thread() {
    pthread_t trun;
    int r = pthread_create(&trun,NULL,run_thread, NULL);
    if (r!=0) {
        cerr << "ERROR: cannot create thread" << endl;
        run = false;
    }
}


extern "C" void waitfor_connected() {
    while (!isConnected){
        usleep(500000);
    }
}




void exec_move(float x_m) {

	double tv_nom = tv_good; if (x_m < 0) tv_nom *= -1;
	double current_x = robot.x(), current_y = robot.y();
	
	double dx = fabs(x_m) - sqrt((robot.x()-current_x)*(robot.x()-current_x)+(robot.y()-current_y)*(robot.y()-current_y));
	
	while (dx>0.01) {
		double tv = tv_nom;
        if (dx<0.2) { tv = tv_nom*dx/0.2; }
        if (fabs(tv)<0.1) { tv = 0.1*tv/fabs(tv); }
        setSpeed(tv, 0.0);
        wait(0.1);
        dx = fabs(x_m) - sqrt((robot.x()-current_x)*(robot.x()-current_x)+(robot.y()-current_y)*(robot.y()-current_y));
        //cout << "MOVE -- " << " VEL: " << last_speed_tv << " " << last_speed_rv << "  POS: " << robot.x() << " " << robot.y() << " " << RAD2DEG(robot.theta()) << endl;
        //cout << "dx = " << dx << " x_m = " << x_m << endl;
    }
    stop();
    wait(0.25);
}


double norm_target_angle(double a) {
    if (fabs(NORM_PI(a-0))<0.15)
        return 0;
    else if (fabs(NORM_PI(a-M_PI/2))<0.15)
        return M_PI/2;
    else if (fabs(NORM_PI(a-M_PI))<0.15)
        return M_PI;
    else if (fabs(NORM_PI(a-3*M_PI/2))<0.15)
        return 3*M_PI/2;
    else
        return a;
}


void exec_turn(double th_deg) {
    double current_th = robot.theta();
    double target_th = norm_target_angle(current_th + DEG2RAD(th_deg));
    double rv_nom = rv_good; if (th_deg < 0) rv_nom *= -1;
    double dth = fabs(NORM_PI(robot.theta()-target_th));
    //cout << "DTH -- " << dth << endl;
    while (dth>0.03) {
        double rv = rv_nom;
        if (dth<0.5) { rv = rv_nom*dth/0.5; }
        if (fabs(rv)<0.3)
            rv = 0.3*rv/fabs(rv);
        setSpeed(0.0, rv);
        wait(0.1);
        dth = fabs(NORM_PI(robot.theta()-target_th));
        // cout << "TURN -- " << " VEL: " << last_speed_tv << " " << last_speed_rv << "  POS: " << robot.x() << " " << robot.y() << " " << RAD2DEG(robot.theta()) << endl;
        // cout << "DTH -- " << dth << " -- target " << target_th << endl;
    }
    stop();
    wait(0.25);
}

extern "C" void left(int r=1) {
    exec_turn(90*r);
}

extern "C" void right(int r=1) {
    exec_turn(-90*r);
}

extern "C" void forward(int r=1) {
    exec_move(move_step*r);
}

extern "C" void backward(int r=1) {
    exec_move(-move_step*r);
}







